//
//  ViewController.swift
//  ParallaxMotion
//
//  Created by Jakub Ziembiński on 11/04/2017.
//  Copyright © 2017 Jakub Ziembiński. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    final let BUTTON_FRAME = CGRect(x: 110, y: 380, width: 100, height: 100)
    final let BUTTON_DRAG_HORIZONTAL: CGFloat = 100
    final let BUTTON_DRAG_VERTICAL: CGFloat = 80

    @IBOutlet weak var background: UIImageView!
    var buttonInitialPosition: CGPoint?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let button = UIButton(frame: BUTTON_FRAME)
        button.setImage(#imageLiteral(resourceName: "redButton"), for: .normal)
        button.addTarget(self, action: #selector(dragInside(_:forEvent:)), for: .touchDragInside)
        button.addTarget(self, action: #selector(dragEnded(_:)), for: .touchUpInside)
        button.addTarget(self, action: #selector(dragEnded(_:)), for: .touchDragExit)
        view.addSubview(button)
        buttonInitialPosition = button.center
        
        applyMotionEffect(toView: background, magnitude: 30)
        
    }
    
    func vsEllipse(point: CGPoint, center: CGPoint) -> CGFloat {
        
        let ellipseX = pow((point.x - center.x), 2)/pow(100, 2)
        let ellipseY = pow((point.y - center.y), 2)/pow(80, 2)
        
        return ellipseX + ellipseY
    }
    
//    func pointOnEllipse() -> CGPoint {
//        
//        let x: CGFloat
//        let y: CGFloat
//        
//        guard let buttonInitPos = buttonInitialPosition else { return CGPoint(x: 0, y: 0) }
//        
//        
//        return CGPoint(x: 0, y: 0)
//    }
    
    func dragInside(_ sender: Any, forEvent event: UIEvent) {
        guard let button = sender as? UIButton else { return }
        
        if let touch: UITouch = event.touches(for: button)?.first,
            let buttonInitPos = buttonInitialPosition {
            let touchCenter = touch.location(in: view)
            let buttonCenter = button.center
            
            let result = vsEllipse(point: buttonCenter, center: buttonInitPos)
            
            if result <= 1.0 {
                button.center = touchCenter
            } else {
                // button.center = punkt wspólny lini prostej z buttonInitPos do touchCenter oraz elipsy
            }
        }
    }
    
    func dragEnded(_ sender: Any) {
        guard let button = sender as? UIButton else { return }
        if let initial = self.buttonInitialPosition {
            if (button.center.y - initial.y) > 39 {
//                self.background.motionEffects = []
            } else if (initial.y - button.center.y) > 100 {
                UIButton.animate(withDuration: 0.5, animations: { 
                    button.transform = CGAffineTransform(scaleX: 1.2, y: 1.2)
                }, completion: { (true) in
                    UIButton.animate(withDuration: 0.3, animations: { 
                        button.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
                    })
                })
            }
            UIView.animate(withDuration: 0.2, animations: {
                    button.center = initial
            })
        }
    }
    
    func applyMotionEffect(toView view: UIView, magnitude: Float) {
        let xMotion = UIInterpolatingMotionEffect(keyPath: "center.x", type: .tiltAlongHorizontalAxis)
        let yMotion = UIInterpolatingMotionEffect(keyPath: "center.y", type: .tiltAlongVerticalAxis)
        
        xMotion.minimumRelativeValue = -magnitude
        xMotion.maximumRelativeValue = magnitude
        
        yMotion.minimumRelativeValue = -magnitude
        yMotion.maximumRelativeValue = magnitude
        
        let group = UIMotionEffectGroup()
        group.motionEffects = [xMotion, yMotion]
        
        view.addMotionEffect(group)
    }
}

